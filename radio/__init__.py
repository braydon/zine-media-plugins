# -*- coding: utf-8 -*-
"""
A widget to display information from a radio stream, and to present links
to be able to connect and listen to the stream. 

:copyright: (c) 2009 by Future Roots, Inc., see AUTHORS for more details.
:license: BSD, see LICENSE for more details.
"""

from os.path import join, dirname
import urllib2
import re
from werkzeug import escape, url_encode
import simplejson
from zine.api import url_for, _
from zine.widgets import Widget
from zine.views.admin import render_admin_response
from zine.privileges import BLOG_ADMIN, require_privilege
from zine.utils.admin import flash
from zine.utils.validators import ValidationError, check, is_valid_url
from zine.utils.http import redirect_to
from zine.utils import forms

TEMPLATE_FILES = join(dirname(__file__), 'templates')
SHARED_FILES = join(dirname(__file__), 'shared')

radio_types = {
    u'shoutcast': _('Shoutcast'),
}

def has_trailing_slash(message=None):
    """A validator that validates for a trailing slash."""

    def validate(form, uri):
        if not uri[-1] == "/":
            message = _('The URI is missing a trailing forward slash. Please change to: %s' % (uri+"/"))
            raise ValidationError(message)
    return validate

class RadioStreamForm(forms.Form):
    """The configuration form."""
    title = forms.TextField(_('Title'), max_length=20, required=True)
    description = forms.TextField(_('Description'), max_length=300, required=True, widget=forms.Textarea)
    base_uri = forms.TextField(_('Base URI'), validators=[is_valid_url(), has_trailing_slash()], 
                               max_length=100, required=True)
    type = forms.ChoiceField(_('Radio type'))

    def __init__(self, initial=None):
        forms.Form.__init__(self, initial)
        choices = sorted(radio_types.items(), key=lambda x: x[1].lower())
        self.fields['type'].choices = choices


def add_radio_link(req, navigation_bar):
    if req.user.has_privilege(BLOG_ADMIN):
        for link_id, url, title, children in navigation_bar:
            if link_id == 'options':
                children.insert(-3, ('radio',
                                     url_for('radio/config'),
                                     _('Radio')))

@require_privilege(BLOG_ADMIN)
def show_radio_config(req):
    """Show the radio control panel."""
    stream = req.app.cfg['radio/streams']
    if stream:
        stream_loads = simplejson.loads(stream)
    else:
        stream_loads = None
    form = RadioStreamForm(initial=stream_loads)

    if req.method == 'POST' and form.validate(req.form):
        if form.has_changed:
            stream = {
                'title': form['title'],
                'description' : form['description'],
                'base_uri': form['base_uri'],
                'type': form['type']
                }
            req.app.cfg.change_single('radio/streams', simplejson.dumps(stream))

            if req.app.cfg['radio/streams']:
                flash(_('Radio has been successfully updated.'), 'ok')
            else:
                flash(_('Radio disabled.'), 'ok')
        return redirect_to('radio/config')
    return render_admin_response('admin/configure_radio.html',
                                 'options.radio',
                                 form=form.as_widget())

class RadioWidget(Widget):
    NAME = 'radio_widget'
    TEMPLATE = 'radio_widget.html'

    def __init__(self, show_title=False, title='Radio Widget'):
        self.show_title = show_title
        self.title = title
        self.info = get_info()

    @staticmethod
    def get_display_name(req):
        return _('Internet Radio Station Info')

head_re = re.compile('^.*<body>')
foot_re = re.compile('</body>.*')

def get_info():
    stream = simplejson.loads(get_application().cfg['radio/streams'])
    if stream["type"] == "shoutcast":
        r = urllib2.Request(stream["base_uri"]+"7", headers={"User-Agent":"Mozilla/5.0 (X11; U; Linux i686) Gecko/20071127 Firefox/2.0.0.11"})
        data = urllib2.urlopen(r).read()
        data = re.sub(head_re, "", data)
        data = re.sub(foot_re, "", data)
        data = data.split(",",7)
        info = {}
        for n, attr in enumerate(["current", "status", "peak", "max", "reported", "bitrate", "track"]):
            info[attr] = data[n]
        return info
    else:
        return "Unsupported Radio Type: %s" % (stream["type"])

def setup(app, plugin):
    app.add_config_var('radio/streams', forms.TextField(default=u''))
    app.add_url_rule('/options/radio', prefix='admin', endpoint='radio/config', view=show_radio_config)
    app.add_servicepoint('radio/get_info', get_info)
    app.add_widget(RadioWidget)
    app.connect_event('modify-admin-navigation-bar', add_radio_link)
    app.add_template_searchpath(TEMPLATE_FILES)
    app.add_shared_exports('radio', SHARED_FILES)
