# -*- coding: utf-8 -*-
"""
A plugin to be able to feature posts. 

:copyright: (c) 2009 by Future Roots, Inc., see AUTHORS for more details.
:license: BSD, see LICENSE for more details.
"""

from os.path import join, dirname
import urllib2
import re
from werkzeug import escape, url_encode, Response
import simplejson
from zine.api import url_for, _
from zine.widgets import Widget
from zine.views.admin import render_admin_response, PER_PAGE
from zine.privileges import assert_privilege, require_privilege, \
     CREATE_ENTRIES, EDIT_OWN_ENTRIES, EDIT_OTHER_ENTRIES, \
     CREATE_PAGES, EDIT_OWN_PAGES, EDIT_OTHER_PAGES, MODERATE_COMMENTS, \
     MANAGE_CATEGORIES, BLOG_ADMIN
from zine.utils.admin import flash, load_zine_reddit, require_admin_privilege
from zine.utils.validators import ValidationError, check, is_valid_url
from zine.utils.pagination import AdminPagination
from zine.utils.http import redirect_to
from zine.utils import forms
from zine.application import add_link, add_script
from sqlalchemy import sql
from zine.database import db, metadata, posts, comments, post_categories, categories
from zine.models import Post, PostQuery
from zine.utils import forms
from werkzeug.exceptions import NotFound
from zine import models

TEMPLATE_FILES = join(dirname(__file__), 'templates')
SHARED_FILES = join(dirname(__file__), 'shared')
SHARED_FILES_CORE = join(dirname(__file__), 'shared/core')

class FeaturedPostQuery(PostQuery):
    def type(self, content_type):
        """Filter all posts by a given type."""
        return self.filter_by(content_type=content_type)    


class FeaturedPost(Post):
    """This is used to artificially extend the Post with an extra
    property called weight. This is mapped to a joined table of both
    posts and post_featured.
    """

    query = db.query_property(FeaturedPostQuery)    

    def __init__(self, title, author, text, slug=None, pub_date=None,
                 last_update=None, comments_enabled=True,
                 pings_enabled=True, status=models.STATUS_PUBLISHED,
                 parser=None, uid=None, content_type='entry', weight=False):

        super(Post, self).__init__(title, author, text, slug, pub_date,
                                  last_update, coments_enabled, pings_enabled,
                                  status, parser, uid, content_type)
        self.weight = weight


class FeaturedQuery(db.Query):
    pass

class Featured(object):
    """ This is the actual featured model, if all we want is a list of
    the post_id(s) a.k.a. id(s) then we use this. This is also primarily used
    for adding features without having to deal with the post itself."""

    query = db.query_property(FeaturedQuery)

    def __init__(self, id, weight):
        self.id = id
        self.weight = weight

#here we define the new table to store featured post data
post_featured = db.Table('post_featured', metadata,
    db.Column('post_id', db.Integer, db.ForeignKey('posts.post_id'), nullable=False, primary_key=True),
    db.Column('weight', db.Integer),
)

#here we join the posts table and the post_featured table
post_featured_joined = sql.outerjoin(posts, post_featured, posts.c.post_id == post_featured.c.post_id)

#here we map the joined table with our 'joined' model.
db.mapper(FeaturedPost, post_featured_joined, properties={
        'id':[posts.c.post_id, post_featured.c.post_id],
        'author': db.relation(models.User),
        'categories': db.relation(models.Category, secondary=post_categories, lazy=False,
                                    order_by=[db.asc(categories.c.name)]),
        'comments': db.relation(models.Comment),
        }, order_by=[post_featured.c.weight])

#here we map our featured model with it's table
db.mapper(Featured, post_featured, properties={
        'id': post_featured.c.post_id,
        }, order_by=[post_featured.c.weight])

def init_database():
    """ This is primarily for inserting the new 
    table called post_featured. """
    from zine.application import get_application
    engine = get_application().database_engine
    metadata.create_all(engine)


def add_featured_link(req, navigation_bar):
    """ We add another link in the Manage navigation 
    bar called Featured"""
    if req.user.has_privilege(BLOG_ADMIN):
        for link_id, url, title, children in navigation_bar:
            if link_id == 'manage':
                children.insert(0, ('featured',
                                     url_for('admin/featured'),
                                     _('Featured')))

def inject_featured(req, context):
    """ We add in some nessasary css, and js."""
    add_link('stylesheet', url_for('featured/shared',
                                   filename='featured.css'), 'text/css')    
    add_script(url_for('featured/shared', filename='featured.js'))

def validate_post_id(message=None):
    """ This validates that there is a post with the id."""
    def validate(form, post_id):
        post = Post.query.get(post_id)
        if post is None:
            message = _('There is not a post with the id: %s' % (post_id))
            raise ValidationError(message)
    return validate

class FeaturedForm(forms.Form):
    """ This is used for inserting new post features."""
    post_id = forms.IntegerField(_('Post ID'), validators=[validate_post_id()])
    weight = forms.IntegerField(_('Weight'))

    def make_featured(self):
        """A helper function that creates a post object from the data."""
        data = self.data
        return Featured(data['post_id'], data['weight'])

    def save_changes(self):
        """Apply the changes."""
        self.post_id = self.data['post_id']
        self.weight = self.data['weight']

class DeleteFeaturedForm(forms.Form):
    """ For removing post features"""
    pass

@require_privilege(BLOG_ADMIN)
def show_featured(request):
    """ This is the view for modifying post features."""
    entry_query = FeaturedPost.query.type('entry').filter(FeaturedPost.weight >= 0)
    entries = entry_query.order_by([FeaturedPost.weight]) 

    return render_admin_response('admin/featured.html', 'manage.featured', 
                                 entries=entries)

@require_privilege(BLOG_ADMIN)
def add_featured(request, post_id, weight):
    """ This is the view for adding featured posts."""
    featured = Featured.query.get(post_id)
    if featured == None:
        featured = Featured(post_id, weight)
        db.save(featured)
        db.commit()
        return Response("Success")

@require_privilege(BLOG_ADMIN)
def remove_featured(request, post_id):
    """ This is the view for removing featured posts."""
    featured = Featured.query.get(post_id)
    db.delete(featured)
    db.commit()
    return Response("Success")

@require_privilege(BLOG_ADMIN)
def modify_featured_weights(request):
    """ This is to modify multiple features' weights at a time."""
    for key, value in simplejson.load(request.form["weights"]).iteritems():
        featured = Featured.query.get(key)
        featured.weight = value
    db.commit()
    return Response("Success")

@require_admin_privilege(CREATE_ENTRIES | EDIT_OWN_ENTRIES | EDIT_OTHER_ENTRIES)
def manage_featured_entries(request, page):
    """Show a list of entries."""
    entry_query = FeaturedPost.query.type('entry')
    entries = entry_query.order_by([Post.status, Post.pub_date.desc()]) \
                         .limit(PER_PAGE).offset(PER_PAGE * (page - 1)).all()
    pagination = AdminPagination('admin/manage_entries', page, PER_PAGE,
                                 entry_query.count())
    if not entries and page != 1:
        raise NotFound()
    return render_admin_response('admin/manage_entries.html', 'manage.entries',
                                 entries=entries, pagination=pagination)

def setup(app, plugin):
    #XXX We may want to not automatically do this table creation?
    init_database()
    app.connect_event('before-admin-response-rendered', inject_featured)
    app.add_url_rule('/featured', prefix='admin', endpoint='admin/featured', 
                     view=show_featured),
    app.add_url_rule('/add_featured/<int:post_id>', prefix='admin', endpoint='featured/add', 
                     view=add_featured, defaults={'weight':0})
    app.add_url_rule('/remove_featured/<int:post_id>', prefix='admin', endpoint='featured/remove', 
                     view=remove_featured)
    app.connect_event('modify-admin-navigation-bar', add_featured_link)
    app.add_template_searchpath(TEMPLATE_FILES)
    app.add_shared_exports('featured', SHARED_FILES)
    app.add_view('admin/manage_entries', manage_featured_entries)
