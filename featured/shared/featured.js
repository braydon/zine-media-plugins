$(document).ready(function(){
$("a[ref=feature]").click(function(){
var elm = $(this)
var url = elm.attr("href");
function toggle(){
p = elm.parent().parent();
if (p.hasClass("on")){ p.removeClass("on"); p.addClass("off") }
else { p.removeClass("off"); p.addClass("on"); }
}
$.get(url, toggle);
return false;});
Sortable.init('featured_entries');
});

var sortable_id = null;
Sortable = {
init: function(id) {
elm = document.getElementById(id);
sortable_id = elm.id;
this.makeListSortable(elm, this.setHandle)
},
coordinates : function() {
return Sortable._coordinatesFactory
},
events : function() {
return Sortable._eventsFactory
},
css : function() {
return Sortable._cssFactory
},
findHandle : function(item) {
var children = jQuery(item).children()
for (var i = 0; i < children.length; i++) {
var child = children[i]
if (child.getAttribute("class") == null) continue
if (child.getAttribute("class").indexOf("handle") >= 0)
return child
}
return item
},
setHandle : function(item) {
item.SortableDragGroup.setHandle(Sortable.findHandle(item))
},
getElementsByClassName : function(classname, node) {
if(!node) node = document.getElementsByTagName("body")[0];
var a = [];
var re = new RegExp('\\b' + classname + '\\b');
var els = jQuery(node).children();
for(var i=0,j=els.length; i<j; i++)
if(re.test(els[i].className))a.push(els[i]);
return a;
},
serializeList : function(list) {
var items = Sortable.getElementsByClassName("sortable",list)
var array = new Array()
for (var i = 0, n = items.length; i < n; i++) {
var item = items[i]
array.push(this._identifier(item))
}
return array.join(',')
},
inspectListOrder : function(id) {
return this.serializeList(document.getElementById(id))
},
_identifier : function(item) {
var trim = this.trim
var identifier
identifier = trim(item.getAttribute("id"))
if (identifier != null && identifier.length > 0) return identifier;
identifier = trim(item.getAttribute("itemID"))
if (identifier != null && identifier.length > 0) return identifier;
return trim(item.innerHTML)
},
_itemsByID : function(list) {
var array = new Array()
var items = list.getElementsByTagName('li')
for (var i = 0, n = items.length; i < n; i++) {
var item = items[i]
array[Sortable._identifier(item)] = item
}
return array
},
trim : function(text) {
if (text == null) return null
return text.replace(/^(\s+)?(.*\S)(\s+)?$/, '$2')
},
makeSortable : function(item) {
var group = Sortable.createSimpleGroup(item)
group.register('dragstart', this._onDragStart)
group.register('dragmove', this._onDragMove)
group.register('dragend', this._onDragEnd)
return group
},
makeListSortable : function(list) {
var coordinates = Sortable.coordinates()
var items = Sortable.getElementsByClassName("sortable",list)
Sortable.helpers.map(items, function(item) {
var dragGroup = Sortable.makeSortable(item)
dragGroup.setThreshold(4)
var min, max
dragGroup.addTransform(function(coordinate, dragEvent) {
return coordinate.constrainTo(min, max)
})
dragGroup.register('dragstart', function() {
var items = Sortable.getElementsByClassName("sortable",list)
min = max = coordinates.topLeftOffset(items[0])
for (var i = 1, n = items.length; i < n; i++) {
var offset = coordinates.topLeftOffset(items[i])
min = min.min(offset)
max = max.max(offset)
}
})
})
for (var i = 1, n = arguments.length; i < n; i++)
Sortable.helpers.map(items, arguments[i])
},
_onDragStart : function(dragEvent) {
},
_onDragMove : function(dragEvent) {
var coordinates = Sortable.coordinates()
var item = dragEvent.group.element
var xmouse = dragEvent.transformedMouseOffset
var moveTo = null
var previous = Sortable.helpers.previousItem(item, item.nodeName)
while (previous != null) {
var bottomRight = coordinates.bottomRightOffset(previous)
if (xmouse.y <= bottomRight.y && xmouse.x <= bottomRight.x) {
moveTo = previous
}
previous = Sortable.helpers.previousItem(previous, item.nodeName)
}
if (moveTo != null) {
Sortable.helpers.moveBefore(item, moveTo)
return
}
var next = Sortable.helpers.nextItem(item, item.nodeName)
while (next != null) {
var topLeft = coordinates.topLeftOffset(next)
if (topLeft.y <= xmouse.y && topLeft.x <= xmouse.x) {
moveTo = next
}
next = Sortable.helpers.nextItem(next, item.nodeName)
}
if (moveTo != null) {
Sortable.helpers.moveBefore(item, Sortable.helpers.nextItem(moveTo, item.nodeName))
return
}
},
_onDragEnd : function(dragEvent) {
object_oids = Sortable.inspectListOrder(sortable_id);
jQuery.post("weights",{weights: object_oids });
Sortable.coordinates().create(0, 0).reposition(dragEvent.group.element)
},
createSimpleGroup : function(element, handle) {
handle = handle ? handle : element
var group = this.createGroup(element)
group.setHandle(handle)
group.drag()
group.onTopWhileDragging()
return group
},
createGroup : function(element) {
var group = new _SortableDragGroup(this, element)
var position = Sortable.css().readStyle(element, 'position')
if (position == 'static') {
element.style["position"] = 'relative'
} else if (position == 'absolute') {
/* for Safari 1.2 */
Sortable.coordinates().topLeftOffset(element).reposition(element)
}
// TODO: only if Sortable.isDebugging()
group.register('draginit', this._showDragEventStatus)
group.register('dragmove', this._showDragEventStatus)
group.register('dragend', this._showDragEventStatus)
return group
},
_showDragEventStatus : function(dragEvent) {
window.status = dragEvent.toString()
},
_createEvent : function(type, event, group) {
return new _SortableDragEvent(type, event, group)
},
vertical_constraint : function() {
return function(coordinate, dragEvent) {
var x = dragEvent.topLeftOffset.x
return coordinate.x != x
? coordinate.factory.create(x, coordinate.y) 
: coordinate
}
},
horizontal_contraint : function() {
return function(coordinate, dragEvent) {
var y = dragEvent.topLeftOffset.y
return coordinate.y != y
? coordinate.factory.create(coordinate.x, y) 
: coordinate
}
},
}
Sortable._coordinatesFactory = {
create : function(x, y) {
return new _SortableCoordinate(this, x, y)
},
origin : function() {
return this.create(0, 0)
},
topLeftPosition : function(element) {
var left = parseInt(Sortable.css().readStyle(element, "left"))
var left = isNaN(left) ? 0 : left
var top = parseInt(Sortable.css().readStyle(element, "top"))
var top = isNaN(top) ? 0 : top
return this.create(left, top)
},
bottomRightPosition : function(element) {
return this.topLeftPosition(element).plus(this._size(element))
},
topLeftOffset : function(element) {
var offset = this._offset(element) 
var parent = element.offsetParent
while (parent) {
offset = offset.plus(this._offset(parent))
parent = parent.offsetParent
}
return offset
},
bottomRightOffset : function(element) {
return this.topLeftOffset(element).plus(
this.create(element.offsetWidth, element.offsetHeight))
},
scrollOffset : function() {
if (window.pageXOffset) {
return this.create(window.pageXOffset, window.pageYOffset)
} else if (document.documentElement) {
return this.create(
document.body.scrollLeft + document.documentElement.scrollLeft, 
document.body.scrollTop + document.documentElement.scrollTop)
} else if (document.body.scrollLeft >= 0) {
return this.create(document.body.scrollLeft, document.body.scrollTop)
} else {
return this.create(0, 0)
}
},
clientSize : function() {
if (window.innerHeight >= 0) {
return this.create(window.innerWidth, window.innerHeight)
} else if (document.documentElement) {
return this.create(document.documentElement.clientWidth,
document.documentElement.clientHeight)
} else if (document.body.clientHeight >= 0) {
return this.create(document.body.clientWidth,
document.body.clientHeight)
} else {
return this.create(0, 0)
}
},
mousePosition : function(event) {
event = Sortable.events().fix(event)
return this.create(event.clientX, event.clientY)
},
mouseOffset : function(event) {
event = Sortable.events().fix(event)
if (event.pageX >= 0 || event.pageX < 0) {
return this.create(event.pageX, event.pageY)
} else if (event.clientX >= 0 || event.clientX < 0) {
return this.mousePosition(event).plus(this.scrollOffset())
}
},
_size : function(element) {
return this.create(element.offsetWidth, element.offsetHeight)
},
_offset : function(element) {
return this.create(element.offsetLeft, element.offsetTop)
}
}
function _SortableCoordinate(factory, x, y) {
this.factory = factory
this.x = isNaN(x) ? 0 : x
this.y = isNaN(y) ? 0 : y
}
_SortableCoordinate.prototype = {
toString : function() {
return "(" + this.x + "," + this.y + ")"
},
plus : function(that) {
return this.factory.create(this.x + that.x, this.y + that.y)
},
minus : function(that) {
return this.factory.create(this.x - that.x, this.y - that.y)
},
min : function(that) {
return this.factory.create(
Math.min(this.x , that.x), Math.min(this.y , that.y))
},
max : function(that) {
return this.factory.create(
Math.max(this.x , that.x), Math.max(this.y , that.y))
},
constrainTo : function (one, two) {
var min = one.min(two)
var max = one.max(two)
return this.max(min).min(max)
},
distance : function (that) {
return Math.sqrt(Math.pow(this.x - that.x, 2) + Math.pow(this.y - that.y, 2))
},

reposition : function(element) {
element.style["top"] = this.y + "px"
element.style["left"] = this.x + "px"
},
}
Sortable._eventsFactory = {
fix : function(event) {
if (!event) event = window.event

if (event.target) {
if (event.target.nodeType == 3) event.target = event.target.parentNode
} else if (event.srcElement) {
event.target = event.srcElement
}
return event
},
register : function(element, type, func) {
if (element.addEventListener) {
element.addEventListener(type, func, false)
} else if (element.attachEvent) {
if (!element._listeners) element._listeners = new Array()
if (!element._listeners[type]) element._listeners[type] = new Array()
var workaroundFunc = function() {
func.apply(element, new Array())
}
element._listeners[type][func] = workaroundFunc
element.attachEvent('on' + type, workaroundFunc)
}
},
unregister : function(element, type, func) {
if (element.removeEventListener) {
element.removeEventListener(type, func, false)
} else if (element.detachEvent) {
if (element._listeners 
&& element._listeners[type] 
&& element._listeners[type][func]) {

element.detachEvent('on' + type, 
element._listeners[type][func])
}
}
}
}
Sortable._cssFactory = {
readStyle : function(element, property) {
if (element.style[property]) {
return element.style[property]
} else if (element.currentStyle) {
return element.currentStyle[property]
} else if (document.defaultView && document.defaultView.getComputedStyle) {
var style = document.defaultView.getComputedStyle(element, null)
return style.getPropertyValue(property)
} else {
return null
}
}
}
Sortable.helpers = {
map : function(array, func) {
for (var i = 0, n = array.length; i < n; i++) func(array[i])
},
nextItem : function(item, nodeName) {
if (item == null) return
var next = item.nextSibling
while (next != null) {
if (next.nodeName == nodeName) return next
next = next.nextSibling
}
return null
},
previousItem : function(item, nodeName) {
var previous = item.previousSibling
while (previous != null) {
if (previous.nodeName == nodeName) return previous
previous = previous.previousSibling
}
return null
},
moveBefore : function(item1, item2) {
var parent = item1.parentNode
parent.removeChild(item1)
parent.insertBefore(item1, item2)
},
moveAfter : function(item1, item2) {
var parent = item1.parentNode
parent.removeChild(item1)
parent.insertBefore(item1, item2 ? item2.nextSibling : null)
}
}
function _SortableDragEvent(type, event, group) {
this.type = type
this.group = group
this.mousePosition = Sortable.coordinates().mousePosition(event)
this.mouseOffset = Sortable.coordinates().mouseOffset(event)
this.transformedMouseOffset = this.mouseOffset
this.topLeftPosition = Sortable.coordinates().topLeftPosition(group.element)
this.topLeftOffset = Sortable.coordinates().topLeftOffset(group.element)
}
_SortableDragEvent.prototype = {
toString : function() {
return "mouse: " + this.mousePosition + this.mouseOffset + "    " +
"xmouse: " + this.transformedMouseOffset + "    " +
"left,top: " + this.topLeftPosition + this.topLeftOffset
}
}
function _SortableDragGroup(factory, element) {
this.factory = factory
this.element = element
this._handle = null
this._thresholdDistance = 0
this._transforms = new Array()
// TODO: refactor into a helper object, move into events.js
this._listeners = new Array()
this._listeners['draginit'] = new Array()
this._listeners['dragstart'] = new Array()
this._listeners['dragmove'] = new Array()
this._listeners['dragend'] = new Array()
}
_SortableDragGroup.prototype = {
/*
* TODO:
*   - unregister(type, func)
*   - move custom event listener stuff into Event library
*   - keyboard nudging of "selected" group
*/
setHandle : function(handle) {
var events = Sortable.events()
handle.SortableDragGroup = this
events.register(handle, 'mousedown', this._dragInit)
handle.onmousedown = function() { return false }
if (this.element != handle)
events.unregister(this.element, 'mousedown', this._dragInit)
},
register : function(type, func) {
this._listeners[type].push(func)
},
addTransform : function(transformFunc) {
this._transforms.push(transformFunc)
},
verticalOnly : function() {
this.addTransform(Sortable.vertical_contraint())
},
horizontalOnly : function() {
this.addTransform(Sortable.horizontal_constraint())
},
setThreshold : function(thresholdDistance) {
this._thresholdDistance = thresholdDistance
},
drag : function(opacity) {
this.register('dragstart', function(dragEvent) {
var element = dragEvent.group.element
element.style.filter = 'alpha(opacity=' + (opacity * 100) + ')'
})
this.register('dragend', function(dragEvent) {
var element = dragEvent.group.element
element.style.filter = 'alpha(opacity=100)'
})
},
onTopWhileDragging : function(zIndex) {
var zIndex = typeof(zIndex) != "undefined" ? zIndex : 100000;
var originalZIndex = Sortable.css().readStyle(this.element, "z-index")
this.register('dragstart', function(dragEvent) {
dragEvent.group.element.style.zIndex = zIndex
})
this.register('dragend', function(dragEvent) {
dragEvent.group.element.style.zIndex = originalZIndex
})
},
_dragInit : function(event) {
event = Sortable.events().fix(event)
var group = document.SortableDragGroup = this.SortableDragGroup
var dragEvent = group.factory._createEvent('draginit', event, group)
group._isThresholdExceeded = false
group._initialMouseOffset = dragEvent.mouseOffset
group._grabOffset = dragEvent.mouseOffset.minus(dragEvent.topLeftOffset)
Sortable.events().register(document, 'mousemove', group._drag)
document.onmousemove = function() { return false }
Sortable.events().register(document, 'mouseup', group._dragEnd)
group._notifyListeners(dragEvent)
},
_drag : function(event) {
event = Sortable.events().fix(event)
var coordinates = Sortable.coordinates()
var group = this.SortableDragGroup
if (!group) return
var dragEvent = group.factory._createEvent('dragmove', event, group)
var newTopLeftOffset = dragEvent.mouseOffset.minus(group._grabOffset)
// TODO: replace with DragThreshold object
if (!group._isThresholdExceeded) {
var distance = 
dragEvent.mouseOffset.distance(group._initialMouseOffset)
if (distance < group._thresholdDistance) return
group._isThresholdExceeded = true
group._notifyListeners(
group.factory._createEvent('dragstart', event, group))
}
for (i in group._transforms) {
var transform = group._transforms[i]
newTopLeftOffset = transform(newTopLeftOffset, dragEvent)
}
var dragDelta = newTopLeftOffset.minus(dragEvent.topLeftOffset)
var newTopLeftPosition = dragEvent.topLeftPosition.plus(dragDelta)
newTopLeftPosition.reposition(group.element)
dragEvent.transformedMouseOffset = newTopLeftOffset.plus(group._grabOffset)
group._notifyListeners(dragEvent)
var errorDelta = newTopLeftOffset.minus(coordinates.topLeftOffset(group.element))
if (errorDelta.x != 0 || errorDelta.y != 0) {
coordinates.topLeftPosition(group.element).plus(errorDelta).reposition(group.element)
}
},
_dragEnd : function(event) {
event = Sortable.events().fix(event)
var group = this.SortableDragGroup
var dragEvent = group.factory._createEvent('dragend', event, group)
group._notifyListeners(dragEvent)
this.SortableDragGroup = null
Sortable.events().unregister(document, 'mousemove', group._drag)
document.onmousemove = null
Sortable.events().unregister(document, 'mouseup', group._dragEnd)
},

_notifyListeners : function(dragEvent) {
var listeners = this._listeners[dragEvent.type]
for (i in listeners) {
listeners[i](dragEvent)
}
}
}
function addEvent(obj, evType, fn, useCapture) {
if (obj.addEventListener){
obj.addEventListener(evType, fn, useCapture);
return true;
} else if (obj.attachEvent){
return obj.attachEvent("on"+evType, fn);
}
}
function removeEvent(obj, evType, fn, useCapture) {
if (obj.removeEventListener) {
obj.removeEventListener(evType, fn, useCapture);
return true;
} else if (obj.detachEvent) {
return obj.detachEvent("on"+evType, fn);
}
}
function mousePos(evt)
{
if(!evt) evt = window.event;
var pos = { 'x': evt.clientX, 'y':evt.clientY };
var b = (window.document.compatMode && window.document.compatMode == "CSS1Compat") ?
window.document.documentElement : window.document.body || null;
if (b){
pos.x += b.scrollLeft;
pos.y +=  b.scrollTop;
}
return pos;
}
function distance(x1,y1,x2,y2){
return Math.sqrt(Math.pow(x1-x2,2)+Math.pow(y1-y2,2));
}
function setOpacity(val){
if (val != null)
{
this.style.opacity = val;
this.style.filter = "alpha(opacity="+(val*100)+")";
}
else
{
this.style.opacity = 1;
this.style.filter = "alpha(opacity=95)";
}
}
function fadeIn(callback)
{
var o=this;
o.cancelFadeOut = true;
o.setOpacity(0.2);
o.style.display="block";
window.setTimeout(
function(){
o.setOpacity(0.5);
},
50);
window.setTimeout(
function(){
o.setOpacity(null);
if (callback) callback();
},
100);
}
function fadeOut(callback)
{
var o=this;
o.cancelFadeOut = false;
o.setOpacity(0.7);
window.setTimeout(
function(){
if (!o.cancelFadeOut) o.setOpacity(0.4);
},
60);
window.setTimeout(
function(){
if (!o.cancelFadeOut) o.setOpacity(0.2);
},
120);		
window.setTimeout(
function(){
if (!o.cancelFadeOut) o.setOpacity(0.1);
},
180);
window.setTimeout(
function(){
if (o.cancelFadeOut) return;
o.setOpacity(null);
o.style.display="none";
if (callback) callback();
},
240);
}

// Messages
function MakeMessages()
{
var message = document.createElement("div");
message.id="message";
message.history = [];
message.setOpacity = setOpacity;
message.fadeIn = fadeIn;
message.fadeOut = fadeOut;

function initDismissHandling(nodismiss)
{return function(){
function dismiss(evt)
{
if (nodismiss) return;
message.initialMousePos = false;
removeEvent(document, "mouseup", dismiss);
removeEvent(document, "keydown", dismiss);
removeEvent(document, "mousemove", checkMouseMove);
message.fadeOut();
}
function checkMouseMove(evt)
{
evt = evt || window.event;
var mp = mousePos(evt);
if (!message.initialMousePos)
message.initialMousePos = mousePos(evt);
if (distance(mp.x,mp.y,message.initialMousePos.x,message.initialMousePos.y) > 10)
dismiss();
}
if (nodismiss) return;
addEvent(document, "mouseup", dismiss);
addEvent(document, "keydown", dismiss);
addEvent(document, "mousemove", checkMouseMove);
}}
message.show = function(theHTML, nodismiss) {
this.innerHTML=theHTML;
this.history.push(theHTML);
this.fadeIn(initDismissHandling(nodismiss));
};
message.hide = function(){
this.fadeOut();
};
message.showFromHistory = function(itemNumber, introducingText)
{
if (itemNumber<this.history.length)
{
var i = this.history.length-1-itemNumber;
var s = this.history[i];
if (introducingText) s = introducingText+s;
this.innerHTML = s;
this.fadeIn(initDismissHandling());
}
else
{
enso.message.show("No message to show.");
}
}
return message;
}